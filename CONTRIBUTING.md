# Contributing

Thanks for your interest to contribute to this project.
There're some rules you need to follow before contributing anything.

Please write your commit messages as helpful to understand as it can be.
Please run tests before pushing your changes.

## Code style

Make sure to enable **editorconfig** plugin in your text editor, IDE, or something you use (next - editor).
It will configure your editor to use these settings:
  * 2 spaces for indentation
  * insert final newline
  * use \\n and not \\r\\n
  * use utf-8

## Documentation style

We use doxygen for writing documentation.

Please write doxygen keywords using a backslash ('\\'), and not using an 'at sign' ('@').
Please write your documentation as helpful as it can be.
